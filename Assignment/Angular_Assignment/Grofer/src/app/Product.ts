export interface IProduct {
  title: string,
  price: number,
  weight: string,
  img: string,
  ids: string
}