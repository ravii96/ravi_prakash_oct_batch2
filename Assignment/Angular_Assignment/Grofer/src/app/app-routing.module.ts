import { NavBarComponent } from './nav-bar/nav-bar.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductCatalougeComponent } from './product-catalouge/product-catalouge.component';
import { ProductCarouselComponent } from './product-carousel/product-carousel.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OfferBannerComponent } from './offer-banner/offer-banner.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';
import { LoginSignupComponent } from './login-signup/login-signup.component';


const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '', component: OfferBannerComponent },
  { path: '', component: ProductCatalougeComponent },
  { path: 'signup', component: LoginSignupComponent },
  { path: 'login', component: LoginSignupComponent },
  { path: 'orders', component: OrderSummaryComponent },
  { path: 'cart', redirectTo: '/' },
  { path: 'checkout', component: CheckoutComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [NavBarComponent, ProductCatalougeComponent, LoginSignupComponent, ProductCarouselComponent, CheckoutComponent, OfferBannerComponent, OrderSummaryComponent];
