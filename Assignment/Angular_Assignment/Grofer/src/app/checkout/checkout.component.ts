import { IProduct } from './../Product';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ProductCatalougeService } from '../product-catalouge.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  errormsg: any;


  constructor(private fb: FormBuilder, private p: ProductCatalougeService, private route: Router) { }

  // **************          validatte form     *************
  checkoutForms = this.fb.group({

    firstName: ['', [Validators.required, Validators.minLength(3)]],
    lastName: ['', [Validators.required, Validators.minLength(3)]],
    email: ['', [Validators.email, Validators.required]],
    address: this.fb.group({
      city: ['', [Validators.maxLength(15)]],
      state: ['', [Validators.required]],
      pincode: ['', [Validators.required]]
    }),
    phoneNumber: ['', [Validators.pattern('[6-9]\\d{9}')]],
    // validate indian phone number
    subscribe: ['']
  })


  //create method to fetch data from form.
  getFirstName() {
    return this.checkoutForms.get('firstName');
  } getLastName() {
    return this.checkoutForms.get('lastName');
  } getEmail() {
    return this.checkoutForms.get('email');
  } getCity() {
    return this.checkoutForms.get('address').get('city');
  } getState() {
    return this.checkoutForms.get('address').get('state');
  } getPincode() {
    return this.checkoutForms.get('address').get('pincode');
  }
  getPhoneNumber() {
    return this.checkoutForms.get('phoneNumber');
  }
  ngOnInit() {
  }


  //Place Order button
  private thankyouFlag = false; // toggle after clicking on place order
  placeorder() {
    this.thankyouFlag = true;
    var orderDetails = this.checkoutForms.value;  // get the form value
    orderDetails.order = this.p.getCartData();    // get the cart item from service 
    var d = new Date();                           
    orderDetails.date = d.toDateString();         // insert date into the orderdetails
    orderDetails.netamount = this.p.getNetAmount();  // get net amount from service class 
    
    // post api call subscribe to sen the data whne click on place order.
    this.p.postApi(orderDetails).subscribe(data => {
      console.log(data);
      this.p.setCartData(null);
    }, error => this.errormsg = error);
  }

}
