import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-login-signup',
  templateUrl: './login-signup.component.html',
  styleUrls: ['./login-signup.component.css']
})
export class LoginSignupComponent implements OnInit {

  constructor(private router: Router, private fb : FormBuilder) { }
  checkUrl(){
    return this.router.url.includes("signup");
  }
  ngOnInit() {
  }
  // validation sholud be add on it.
  loginForm = this.fb.group({

    email: ['', [Validators.email, Validators.required]],
    password: ['']
  });
    // validation sholud be add on it.
  signupForm = this.fb.group({
    username:['',[]],
    email: ['', [Validators.email, Validators.required]],
    password: [''],
    confirm_password: ['']
  });
  loginOperation(){
    console.log(this.loginForm.value);
  }
  signupOperation(){
    console.log(this.signupForm.value);
  }

}
