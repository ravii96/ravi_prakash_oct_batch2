import { RouterModule, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProductCatalougeService } from '../product-catalouge.service';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  private errormsg: "404 Not Found";

  //***********  p is a instance of service classs ***************
  constructor(private p: ProductCatalougeService, private route: Router) { }
  private sheet; //  used for style binding sidebar popup
  private productArray;     // contains items on cart
  fetchcartData() {
    this.productArray = this.p.getCartData();  // get cart data from service class
  }
  checkout() {
    this.route.navigate(['/checkout']);
  }
  orderSummaryfun() {
    this.route.navigate(['/orders']);
    console.log(this.p.orderSummary);
  }

  //**************** */ calculate total amount of cart
  totalAmount(p) {
    var total: number = 0;
    let x: any;
    for (x in p) {
      total += parseInt(p[x].price) * parseInt(p[x].quant);
    }
    this.p.setNetAmount(total); //  ****** service class method to set the net amount
    return total;
  }

  // calculate Price according to quantity************
  calculatePrice(p) {
    var amt = parseInt(p.price) * parseInt(p.quant);
    return amt;
  }

  // open the popup when click on cart button 
  openNav() {
    this.route.navigate(['/cart']);
    this.fetchcartData();   // fetch cart data  
    
    return this.sheet = { width: '25%', marginLeft: '75%' };  // style binding
  }
  // close the popup when click on X 
  closeNav() {
    return this.sheet = { width: '0', marginLeft: '0' };  // style binding
  }

  //quantity increment and decrement method
  quantityneg(p) {
    this.p.quantityneg(p);
  }
  quantitypos(p) {
    this.p.quantitypos(p);
  }
  ngOnInit() {
    this.p.orderSummrayApi().subscribe(data => this.p.orderSummary = data, error => this.errormsg = error);
  }
}
