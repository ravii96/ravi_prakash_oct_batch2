import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductCatalougeService } from '../product-catalouge.service';
@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {

  constructor(private route: Router, private p: ProductCatalougeService) { }
  private temp = 0;
  ngOnInit() {
  }
  count() {
    return this.temp++;
  }
  private orderSummary = this.p.orderSummary;

}
