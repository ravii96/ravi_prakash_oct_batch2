import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { IProduct } from './Product';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class ProductCatalougeService {

  //private url: string ="assets/data/product.json";
  private baseurl: string = "http://localhost:3000"; // server api url
  private netAmt: number;

  constructor(private http: HttpClient) { }
  public orderSummary = [];
  
  // get total amount used in order summary component
  getNetAmount() {
    return this.netAmt;
  }
  // set total amount data set from chevkout component
  setNetAmount(amt) {
    this.netAmt = amt;
  }

  // get api call
  getProduct(): Observable<IProduct[]> {
    return this.http.get<IProduct[]>(this.baseurl + "/api/getdata").catch(this.customErrorHandler);
  }
  // error handler
  customErrorHandler(error: HttpErrorResponse) {
    return Observable.throw(error.message || "Server Error");
  }
  // this is used in post api
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  // post api call to sent product details into db 
  postApi(orderDetails): Observable<IProduct[]> {
    return this.http.post<IProduct[]>(this.baseurl + "/api/checkout", orderDetails, this.httpOptions).catch(this.customErrorHandler);
    console.log("sevice :" + orderDetails[0]);
  }

  // api call gor get product data for order summary
  orderSummrayApi() {
    return this.http.get<IProduct[]>(this.baseurl + "/api/order").catch(this.customErrorHandler);
  }
  
  private cart_list = null; // contains cart ditems
  // get cart items
  getCartData() {
    return this.cart_list;
  }

  //set cart items
  setCartData(data) {
    return this.cart_list = data;
  }

  //Quantity button
  quantityneg(p) {
    if (this.cart_list.some(e => e.ids === p.ids)) {
      var index = this.cart_list.findIndex(x => x.ids === p.ids);
      if (this.cart_list[index].quant > 0) {
        this.cart_list[index].quant--;
      }

      this.setCartData(this.cart_list);   // update cart items if quantity decreases
    }
  }
  quantitypos(p) {
    if (this.cart_list.some(e => e.ids === p.ids)) {
      var index = this.cart_list.findIndex(x => x.ids === p.ids);
      this.cart_list[index].quant++;
      this.setCartData(this.cart_list); // update cart items if quantity increases
    }
  }

}