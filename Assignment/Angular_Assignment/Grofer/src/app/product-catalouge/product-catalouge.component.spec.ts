import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCatalougeComponent } from './product-catalouge.component';

describe('ProductCatalougeComponent', () => {
  let component: ProductCatalougeComponent;
  let fixture: ComponentFixture<ProductCatalougeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCatalougeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCatalougeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
