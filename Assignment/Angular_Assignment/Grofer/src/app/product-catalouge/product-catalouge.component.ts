import { IProduct } from './../Product';
import { Component, OnInit } from '@angular/core';
import { ProductCatalougeService } from '../product-catalouge.service';
@Component({
  selector: 'app-product-catalouge',
  templateUrl: './product-catalouge.component.html',
  styleUrls: ['./product-catalouge.component.css']
})
export class ProductCatalougeComponent implements OnInit {

  constructor(private p: ProductCatalougeService) { }
  private products = [];
  private errormsg: string;

  ngOnInit() {
    this.p.getProduct().subscribe(data => this.products = data, error => this.errormsg = error);
  }

  //Add to Cart Button 
  private idmatch;
  public productArray = [];
  toggle(mybtn, p) {
    this.idmatch = mybtn.id;
    var product_html = {
      ids: p.ids,
      price: p.price,
      title: p.title,
      weight: p.weight,
      quant: 1
    }
    if (!this.productArray.some(e => e.ids === p.ids))
      this.productArray.push(product_html);
    this.printArray();
    this.p.setCartData(this.productArray);
  }

  printQuantity(p): string {
    return this.productArray[this.productArray.findIndex(x => x.ids === p.ids)].quant.toString();
  }
  //check array works or not 
  printArray() {
    for (var i = 0; i < this.productArray.length; i++) {
      console.log(this.productArray[i].title + " " + this.productArray[i].price);
    }
  }

  //Quantity button
  quantityneg(p) {
    this.p.quantityneg(p);
  }
  quantitypos(p) {
    this.p.quantitypos(p);
  }
}
