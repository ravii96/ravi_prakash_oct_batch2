
// creating an empty map 
var map1 = new Map(); 

// adding some elements to the map 
map1.set("first name", "ravi"); 
map1.set("last name", "prakash"); 
map1.set("mentor 1","romil");
map1.set("mentor 2","rajat"); 
console.log(map1); 
	
console.log("map1 has mentor 1 ? "+ 
          map1.has("mentor 1")); 
          
console.log("map1 has mentor 3 ? " + 
					map1.has("mentor 3")); 

console.log("get value for key mentor 2 "+ 
					map1.get("mentor 2")); 

// returns undefined 
console.log("get value for key mentor 3 "+ 
					map1.get("mentor 3")); 

console.log("delete element with key mentor 2 "
					+ map1.delete("mentor 2")); 

var get_entries = map1.entries(); 

console.log("Entries"); 
for(var ele of get_entries) 
console.log(ele); 

var get_keys = map1.keys(); 
  
console.log("--------keys----------"); 
for(var ele of get_keys) 
console.log(ele); 
  
var get_values = map1.values(); 

console.log("----------values------------"); 
for(var ele of get_values) 
console.log(ele);


function print(values, key)  
{ 
   console.log(key + "  " + values); 
} 

console.log("-----Print-----"); 
map1.forEach(print); 

map1.clear(); 

// map1 is empty 
console.log(map1); 
