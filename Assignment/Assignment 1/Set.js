/*
Doubt :
not able to take input??????? error readline not define, prompt also not wokred

*/


// Set 
function customSet() {
  this.setArray = [];
  this.length = 0;

  // check existance of an element (Search)
  this.has = function (element) {
    return (this.setArray.indexOf(element) !== -1);
  }

  // return set array
  this.values = function () {
    return this.setArray;
  }

  // add element
  this.add = function (value) {
    if (!this.has(value)) {
      this.setArray.push(value);
      return true;
    }
    return false;
  }

  //find Index
  this.findIndex = function (element) {
    return this.setArray.indexOf(element);
  }

  //remove 
  this.remove = function (element) {
    if (this.has(element)) {
      var indx = this.setArray.indexOf(element);
      this.setArray.splice(indx, 1);
      return true;
    }
    else
      return false;
  }

  //size of set
  this.size = function () {
    return this.setArray.length;
  }

  // give union of set
  this.union = function (otherset) {
    var unionSet = new customSet();
    var set1 = this.values();
    var set2 = otherset.values();
    for (var i = 0; i < set1.size; i++) {
      unionSet.add(set1[i]);      //  add all element in different set
    }
    set2.forEach(element => {
      unionSet.add(element);
    });
    return unionSet;
  }

  // intersection of set 
  this.intersection = function (otherset) {
    var intersectionSet = new customSet();
    var set1 = this.values();
    set1.forEach(element => {
      if (otherset.has(element))   //present in set1 & set2 both
        intersectionSet.add(element);
    });
    return intersectionSet;
  }

  // difference of set
  this.difference = function (otherset) {
    var differenceSet = new customSet();
    var set1 = this.values();
    set1.forEach(element => {
      if (!otherset.has(element))    // if set1 element not present in set2 add that element
        differenceSet.add(element);
    });
    return differenceSet;
  }

  //print
  this.print = function (otherset) {
    var str = this.setArray.join(" ");
    return str;
  }
}


// execution

var firstSet = new customSet();
var secondSet = new customSet();
secondSet.add(2);
secondSet.add(5);
secondSet.add(8);
secondSet.add(1);
secondSet.add(6);
secondSet.add(9);

firstSet.add(1);
firstSet.add(2);
firstSet.add(3);
firstSet.add(4);
firstSet.add(5);
firstSet.add(6);
console.log("Set1 : ");
console.log(firstSet.values())
console.log("Set2 : ");
console.log(secondSet.values());

console.log("Union : ");
console.log(firstSet.union(secondSet).values());
console.log("Intersection : ");
console.log(firstSet.intersection(secondSet).values());
console.log("Difference : ");
console.log(firstSet.difference(secondSet).values());

//var e = prompt("Enter searching element :");
var e = readline();                            // not able to take input??????? error readline not define
console.log("Element Found : 3? " + firstSet.has(e));
// console.log("Enter the number to get the index");
// var e = readline();
// console.log(firstSet.findIndex(e));
console.log("Item remove? " + firstSet.remove(3));
console.log("Item remove? " + firstSet.remove(3));
console.log("Size of set1 : " + firstSet.size());
console.log("Size of set2 : " + secondSet.size());








