
/*
Doubt :
Here Stack is a function constructor or class   ???????????
stackArray  not declared then how can we use this.stackArray in stack constructor?
*/


function Stack(){         // same as -> var Stack = function(){.....}
    this.stackArray =[];    // stackArray  not declared then how its work?
    this.capacity=0;

    //Pop
    this.pop = function()
    {
      if(this.capacity===0)
      {
          return undefined;
      }
      this.capacity--;
      var result = this.stackArray[this.capacity];
      delete this.stackArray[this.capacity];
      return result;
    }

    //push
    this.push= function(value)
    {
        this.stackArray[this.capacity]=value;
        this.capacity++;
    }

    // peek
    this.peek = function()
    {
        return this.stackArray[this.capacity-1];
    }

    // size
    this.size=function()
    {
      return this.capacity;
    }

    // print
    this.printStack = function()
    {
        // for(var i =0;i<this.capacity;i++)
        // {
        //   console.log(this.stackArray[i]+"\t");
        // }

        var str = this.stackArray.join("  ");
        console.log("Stack : "+ str);
    }

}

// execution

var mystack = new Stack();
mystack.push(1);
mystack.push(2);
mystack.push(3);
mystack.printStack();
console.log("Pop : "+ mystack.pop());
mystack.push("Ravi");
console.log("Peek : "+ mystack.peek());
console.log("Pop : "+ mystack.pop());
console.log("Pop : "+ mystack.pop());
console.log("Pop : "+ mystack.pop());
console.log("Pop : "+ mystack.pop());
mystack.printStack();
console.log("Size of Stack : "+ mystack.size());
