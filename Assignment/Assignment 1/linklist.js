
/*
Doubt :
Shows undefined element also in the list when we try to print the list.
*/
//Class Node 
class Node
{
  constructor(data) {
    this.data= data;
    this.next=null;
  }
}

// Linklist Class
function Linklist(){
  this.head=null;
  var length=0;

  //head function 
  this.head = function()
  {
    return this.head;
  }
  // insert function at start
  this.insertAtBeg= function(data)
  {
    var node = new Node(data);
    node.next=this.head;
    this.head=node;
  }

  //insert at end
  this.insertAtEnd= function(data)
  {
    var node = new Node(data);
    if(this.head===null || this.head===undefined)
    {
      this.head=node;
      return;
    }  
    currentNode= this.head;
    while(currentNode.next)
    {
      currentNode= currentNode.next;
    }
    currentNode.next=node;
  }

  //print 
  this.print=function()
  {
    temp =this.head;
    var str ="";
    while(temp!=null)  // when we print Undefined comes in output ?  Why?
    {
      str=str+ "  " +temp.data;
      temp=temp.next;
    }
    return str;
  }

  // remove at start
  this.removeAtBeg= function()
  {
    if(this.head!=null)
    {
      this.head=this.head.next;
    }
    return;
  }

  //remove at end
  this.removeAtEnd = function()
  {
    if(this.head===null)
    {
      return;
    }
    temp=this.head;
    while(temp.next.next!==null)
      temp=temp.next;
    temp.next=null;
  }
}


// execution 

var list = new Linklist();
list.insertAtBeg(10);
list.insertAtBeg(20);
list.insertAtBeg(30);
list.insertAtEnd(40);
list.insertAtEnd(50);
list.insertAtEnd(60);
console.log("List1 : "+list.print());   // show undefined in the list?????
list.removeAtEnd();
console.log("Remove At End: "+list.print());
var list2 = new Linklist();   
list2.insertAtBeg(70);
list2.insertAtBeg(80);
list2.insertAtBeg(90);
console.log("List 2: "+list2.print());
list2.removeAtBeg(90);
console.log("Remove at beg : "+list2.print());

