
function getInputValues()
{
  var regex_Name = /^[a-zA-Z]+$/;
  var regex_loc =/^[0-9a-zA-z ]+$/;
  var regex_email=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/
  var regex_mob=/^[0-9]+$/;
  var form ={};
  form.first_name = document.getElementById("fname").value;
  var isvalid =regex_Name.test(form.first_name);
  if(!isvalid)
  {
    alert("Use only Alphabetic character In First Name");
    return;
  }
    //last name
  form.last_name = document.getElementById("lname").value;
  var isvalid =regex_Name.test(form.last_name);
  if(!isvalid)
  {
    alert("Use only Alphabetic character In Last Name");
    return;
  }

  //location
  form.location = document.getElementById("loc").value;
  var isvalid =regex_loc.test(form.location);
  if(!isvalid)
  {
    alert("Don't use special  character in location");
    return;
  }

  //email
  form.email = document.getElementById("email").value;
  var isvalid =regex_email.test(form.email);
  if(!isvalid)
  {
    alert("Extra special character in Email");
    return;
  }
  

  // Mobile validation
  form.mobile_number = document.getElementById("mnumber").value;
  var isvalid =regex_mob.test(form.mobile_number);
  var len = form.mobile_number.length;
  if(!isvalid || len!==10)
  {
    alert("Use Numeric value of 10 Digits In Mobile Number");
    return;
  }
  console.log(form);
  alert("Submitted Successfully");
}
var flag=true;
function disableEnable()
{
  var form = document.getElementById("form_id");
  var color=document.getElementById("btn");
  if(flag===true)
  {
    for(var i=0;i<form.length;i++)
    {
      form[i].disabled = true;
    }
    color.style.backgroundColor = "#7f0000";
    color.innerHTML="Enable";
    flag=false;
  }
  else
  {
    for(var i=0;i<form.length;i++)
    {
      form[i].disabled = false;
    }
    color.style.backgroundColor = "#000000";
    color.innerHTML="Disable";
    flag=true;
  }
}