var flag = true;
class parent {
  constructor() {
  }
  display(val, ids) {
    console.log(ids);
    document.getElementById(ids).value += val;
  }
  backspace(calc) {
    let size = calc.ScientificResult.value.length;
    calc.ScientificResult.value = calc.ScientificResult.value.substring(0, size - 1);
  }
  switchFunc(ids) {
    if (ids == "scientific") {
      document.getElementById('scientific').style.display = 'block';
      document.getElementById('basic').style.display = 'none';
      console.log("Block : " + ids);
    }
    else {
      document.getElementById('scientific').style.display = 'none';
      document.getElementById('basic').style.display = 'block';
      console.log("Block : " + ids);
    }

  }

  clr(ids) {
    document.getElementById(ids).value = "";
  }

}
class baseCalculator extends parent {
  constructor() {
    super();
  }
  solve(ids) {
    let x = document.getElementById(ids).value
    let y = eval(x);
    setTimeout(() => {
      document.getElementById(ids).value = y;
    }, 1000);
  }
}
class scientificCalculator extends parent {
  constructor() {
    super();
  }
  calculate(calc) {
    if (calc.ScientificResult.value.includes("!")) {

      let size = calc.ScientificResult.value.length;
      let n = Number(calc.ScientificResult.value.substring(0, size - 1));
      let f = 1;

      for (i = 2; i <= n; i++)
        f = f * i;
      setTimeout(() => {
        calc.ScientificResult.value = f;
      }, 1000);

    }
    else if (calc.ScientificResult.value.includes("%")) {

      let size = calc.ScientificResult.value.length;
      let n = Number(calc.ScientificResult.value.substring(0, size - 1));
      setTimeout(function () {
        calc.ScientificResult.value = n / 100;
      }, 1000);

    }

    else {
      setTimeout(function () {
        calc.ScientificResult.value = eval(calc.ScientificResult.value);
      }, 1000);
    }
  }
}
sc = new scientificCalculator();
base = new baseCalculator();
let p = new parent();

