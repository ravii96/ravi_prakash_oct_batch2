const express = require('express');
var cors = require('cors')
const app = express();
const router = express.Router();

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var db_name = "grofer";
var assert = require('assert');
var fs = require('fs');

var coll = "product_list";



app.use(cors());
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));





/// read file to inserted data.
fs.readFile('json/productArray.json', 'utf8', function (err, data) {
  if (err) throw err;

  var json = JSON.parse(data);
  insertRecord(json, coll);
});


// inserted Query
var insertRecord = function (json, coll) {
  MongoClient.connect(url, function (err, db) {
    assert.equal(null, err);
    var dbo = db.db(db_name);
    dbo.collection(coll).deleteMany({}, function () {
      assert.equal(null, err);
      console.log("document deleted");
    })
    dbo.collection(coll).insertMany(json, function (err, res) {
      assert.equal(null, err);
      console.log("1 document inserted");

    });
    db.close();
    //.................. also doing this by traversing array................    
    // for (let p of product_array){
    //   dbo.collection("product_list").insert(p, function(err,res){
    //     if (err) throw err;
    //      console.log("1 document inserted");
    //   });

  });
}


//---------------------Doubt------------------
// var mongoose = require("mongoose");
// mongoose.Promise = global.Promise;
// mongoose.connect("mongodb://localhost:27017/grofer");

var product_array = [];
app.get('/api/getdata', function (req, res) {
  MongoClient.connect(url, function (err, db) {
    assert.equal(null, err);
    var dbo = db.db(db_name);
    dbo.collection(coll).find().toArray(function (err, result) {
      assert.equal(null, err);
      product_array = result;
      db.close();
    });
  });
  res.send(product_array);
});

var orderSummary = [];
app.get('/api/order', function (req, res) {
  MongoClient.connect(url, function (err, db) {
    assert.equal(null, err);
    var dbo = db.db(db_name);
    dbo.collection("order_details").find().toArray(function (err, result) {
      assert.equal(null, err);
      orderSummary = result;
      db.close();
    });
  });
  res.send(orderSummary);
});


app.post('/api/checkout', function (req, res) {
  var orderDetails = req.body;
  console.log("Get data from body :");
  console.log(orderDetails);
  res.status(200);

  MongoClient.connect(url, function (err, db) {
    assert.equal(null, err);
    var dbo = db.db(db_name);
    dbo.collection("order_details").insertOne(orderDetails, function (err, result) {
      assert.equal(null, err);
      product_array = result;
      db.close();
    });
  });

  res.send("ok");

});

app.listen(3000, function () {
  console.log("server Executed");
})
