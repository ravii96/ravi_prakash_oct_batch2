var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Pizza = /** @class */ (function () {
    function Pizza() {
    }
    Pizza.prototype.size = function (val) {
        if (val == "M")
            return 120;
        if (val == "R")
            return 250;
        if (val == "L")
            return 450;
    };
    Pizza.prototype.price = function (size, topping) {
        //console.log("Size Price "+size);
        //console.log("topping Price "+topping);
        var price = size + topping;
        //console.log("Without gst "+ price);
        price = price + (price * 18 / 100);
        return price;
    };
    return Pizza;
}());
var vegPizza = /** @class */ (function (_super) {
    __extends(vegPizza, _super);
    function vegPizza() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    vegPizza.prototype.topping = function (val) {
        if (val === "Mashroom")
            return 70;
        if (val === "Onion")
            return 40;
        if (val === "Double Cheese")
            return 90;
    };
    return vegPizza;
}(Pizza));
var nonVegPizza = /** @class */ (function (_super) {
    __extends(nonVegPizza, _super);
    function nonVegPizza() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    nonVegPizza.prototype.topping = function (val) {
        if (val === "Chicken Mashroom")
            return 70;
        if (val === "Mutton")
            return 90;
        if (val === "Chicken burst")
            return 80;
    };
    return nonVegPizza;
}(Pizza));
var veg = new vegPizza(), nonveg = new nonVegPizza();
var price = veg.price(veg.size("M"), veg.topping("Onion"));
console.log("Veg Pizza Price : " + price);
var price = nonveg.price(nonveg.size("L"), nonveg.topping("Chicken Mashroom"));
console.log("Non-veg Pizza price : " + price);
