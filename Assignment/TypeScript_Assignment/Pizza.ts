class Pizza
{
  size(val: string): number
  {
    if(val=="M")
      return 120;
      
    if(val=="R")
      return 250;
    
    if(val=="L")
      return 450;
  }
  price(size :number, topping: number) :number
  {
    //console.log("Size Price "+size);
    //console.log("topping Price "+topping);
    
    let price :number =size+topping;
    //console.log("Without gst "+ price);
    price= price+(price*18/100);
    return price;
  }
}
class vegPizza extends Pizza
{
  topping(val:string): number
  {
    if(val==="Mashroom")
      return 70;
      
    if(val==="Onion")
      return 40;
      
    if(val==="Double Cheese")
      return 90;
  }
}
class nonVegPizza extends Pizza
{

  topping(val:string): number
  {
    if(val==="Chicken Mashroom")
      return 70;
      
    if(val==="Mutton")
      return 90;
    
    if(val==="Chicken burst")
      return 80;
      
  }
}
var veg = new vegPizza(),
nonveg = new nonVegPizza();
var price : number =veg.price(veg.size("M"),veg.topping("Onion"));
console.log("Veg Pizza Price : "+price);
var price : number =nonveg.price(nonveg.size("L"),nonveg.topping("Chicken Mashroom"));
console.log("Non-veg Pizza price : "+price);